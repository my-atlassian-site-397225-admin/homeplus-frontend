import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';

const AlertDot = styled('div')({
  backgroundColor: '#e53e3e',
  padding: 6,
  borderRadius: 50,
  position: 'absolute',
  right: -5,
  top: -5,
});

const theme = createTheme({
  palette: {
    primary: {
      main: '#52AB98',
      contrastText: '#fff',
      darker: '#438D7D',
    },
    black: {
      main: '#444444',
      contrastText: '#fff',
    },
  },
});

const CustomButton = ({ children, alertDot, ...otherProps }) => (
  <ThemeProvider theme={theme}>
    <Button {...otherProps} sx={{ mt: 1, mr: 1, textDecoration: 'none' }}>
      {alertDot ? <AlertDot /> : null}
      {children}
    </Button>
  </ThemeProvider>
);

CustomButton.propTypes = {
  children: PropTypes.string.isRequired,
  alertDot: PropTypes.bool,
};

export default CustomButton;
