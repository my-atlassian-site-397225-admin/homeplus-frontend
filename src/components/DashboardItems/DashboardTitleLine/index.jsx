import * as React from 'react';
import PropTypes from 'prop-types';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import Box from '@mui/material/Box';
import { ListItemText } from './DashboardTitleLine.style';
import CustomButton from '../../CustomButton';

export default function BasicList({ title, edit, turn }) {
  return (
    <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <List sx={{ width: '95%', bgcolor: 'background.paper' }}>
        <ListItem sx={{ height: '80px' }}>
          <ListItemText>
            <span>{title}</span>
            {edit ? (
              <CustomButton color="primary" variant="outlined">
                Edit
              </CustomButton>
            ) : null}
            {turn ? (
              <CustomButton color="primary" variant="outlined">
                Turn on tasker mode
              </CustomButton>
            ) : null}
          </ListItemText>
        </ListItem>
        <Divider component="li" />
      </List>
    </Box>
  );
}

BasicList.propTypes = {
  title: PropTypes.string.isRequired,
  edit: PropTypes.bool,
  turn: PropTypes.bool,
};
