import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export default function BasicCard() {
  return (
    <Card sx={{ width: 400, height: 250, backgroundColor: '#52AB98' }}>
      <CardContent sx={{ display: 'flex', flexDirection: 'row', gap: '150px' }}>
        <Typography sx={{ fontSize: 30 }}>4</Typography>
      </CardContent>
      <CardContent>
        <Typography sx={{ fontSize: 20 }}>Ongoing Tasks</Typography>
      </CardContent>
    </Card>
  );
}
