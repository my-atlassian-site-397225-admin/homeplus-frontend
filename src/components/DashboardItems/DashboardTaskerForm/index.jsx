import React from 'react';
import Box from '@mui/material/Box';
import CustomButton from '../../CustomButton';
import TaskerInput from './TaskerInput';
import PaymentInput from './PaymentInput';
import CategorySelection from './CategorySelection';

export default function BasicTextFields() {
  return (
    <Box
      component="form"
      sx={{ display: 'flex', flexDirection: 'column', marginLeft: '120px' }}
      noValidate
      autoComplete="off"
    >
      <TaskerInput title="Title" />
      <CategorySelection />
      <TaskerInput title="Skills" />
      <TaskerInput title="Certifications" />
      <TaskerInput title="Introduction" />
      <PaymentInput />

      <Box sx={{ display: 'flex', marginTop: '30px', justifyContent: 'end', width: '70%' }}>
        <CustomButton color="primary" variant="contained">
          Submit
        </CustomButton>
      </Box>
    </Box>
  );
}
