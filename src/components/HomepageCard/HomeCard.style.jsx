import styled from 'styled-components';

export const Card = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  width: 30vh;
  height: 18vh;
  border-radius: 36px;
  overflow: hidden;
  margin: 0 auto;
  &:hover .cardLayer {
    background: rgba(68, 68, 68, 0.5);
  }
  .cardImg {
    display: flex;
    width: 100%;
    height: 100%;
  }

  .cardLayer {
    position: absolute;
    width: 30vh;
    height: 18vh;
    border-radius: 36px;
    background: rgba(68, 68, 68, 0.7);
  }
  .cardText {
    position: absolute;
    display: flex;
    justify-content: center;
    color: white;
    font-size: 30px;
    font-weight: 300;
    z-index: 1;
    font-family: 'Roboto';
  }
`;
